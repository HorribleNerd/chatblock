package com.horriblenerd.chatblock;

import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.ClickEvent;
import net.minecraft.network.chat.HoverEvent;
import net.minecraft.network.chat.TextComponent;
import net.minecraftforge.client.event.ClientChatReceivedEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

// The value here should match an entry in the META-INF/mods.toml file
@Mod("chatblock")
public class ChatBlock {
    // Directly reference a log4j logger.
    private static final Logger LOGGER = LogManager.getLogger();

    public ChatBlock() {

        Config.init();

        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
    }

    @SubscribeEvent
    public void onChat(ClientChatReceivedEvent event) {

        String message = event.getMessage().getString().toLowerCase();

        if (Config.BLOCK_LIST.get().stream().anyMatch(message::contains)) {
            UUID senderUUID = event.getSenderUUID();
            LOGGER.info("Blocking message by: " + senderUUID);
            LOGGER.info('\"' + event.getMessage().getString() + '\"');
            if (Config.NOTIFY_BLOCKED.get()) {
                event.setMessage(new TextComponent("<this message was blocked>").withStyle(ChatFormatting.GRAY)
                        .withStyle((s) -> s.withHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                                        new TextComponent(String.format("%s", senderUUID))))
                                .withClickEvent(new ClickEvent(ClickEvent.Action.COPY_TO_CLIPBOARD, senderUUID == null ? "null" : senderUUID.toString()))));
            }
            else {
                event.setCanceled(true);
            }
        }
    }

}
