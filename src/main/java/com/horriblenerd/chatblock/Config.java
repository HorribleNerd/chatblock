package com.horriblenerd.chatblock;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.config.ModConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HorribleNerd on 06/11/2021
 */
public class Config {

    public static ForgeConfigSpec CLIENT_CONFIG;

    public static ForgeConfigSpec.BooleanValue NOTIFY_BLOCKED;
    public static ForgeConfigSpec.ConfigValue<List<? extends String>> BLOCK_LIST;

    private static final List<String> phrases = new ArrayList<>();

    public static void init() {
        initClient();

        ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, CLIENT_CONFIG);
    }

    private static void initClient() {
        ForgeConfigSpec.Builder builder = new ForgeConfigSpec.Builder();

        builder.comment("Client settings").push("general");

        NOTIFY_BLOCKED = builder.comment("Notify when a message is blocked").define("notify", false);
        BLOCK_LIST = builder.comment("Block list (comma separated, encased in quotation marks and should be lower case)", "[\"test\", \"123\", \"abc\"]").defineList("block_list", phrases, (b) -> b instanceof String);

        builder.pop();
        CLIENT_CONFIG = builder.build();

    }

}
